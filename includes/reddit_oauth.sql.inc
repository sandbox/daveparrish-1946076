<?php

/**
 * @file
 * SQL specific functions
 */

/**
 * Load a Access Token given a Drupal User ID.
 */
function reddit_oauth_access_token_sql_load($uid = NULL) {
  return _reddit_oauth_tokens_load('reddit_access_token', $uid);
}

/**
 * Load a Reddit ID given a Drupal User ID.
 */
function reddit_oauth_reddit_name_sql_load($uid = NULL) {
  return _reddit_oauth_tokens_load('reddit_name', $uid);
}

/**
 * Generic function for loading data from the tokens table.
 */
function _reddit_oauth_tokens_load($row, $uid = NULL) {
  $uid = isset($uid) ? $uid : $GLOBALS['user']->uid;
  $result = db_query("SELECT %s FROM {reddit_oauth_tokens} WHERE uid = %d", $row, $uid);
  $data = db_result($result);
  return $data ? $data : FALSE;
}


/**
 * Remove the Reddit tokens.
 *
 * @return bool
 *   TRUE if the query was successful.
 */
function reddit_oauth_remove_tokens($uid = NULL) {
  $uid = isset($uid) ? $uid : $GLOBALS['user']->uid;
  $result = db_query("DELETE FROM {reddit_oauth_tokens} WHERE uid=%d", $uid);
  return $result ? TRUE : FALSE;
}

/**
 * Load the expiration time for a specific users token.
 *
 * @param int $uid
 *   The users ID. (uid)
 *   If the uid is not specified then use the current logged in users uid.
 */
function reddit_oauth_access_token_expire_time($uid = NULL) {
  // Load the expiration time for the access token.
  $uid = isset($uid) ? $uid : $GLOBALS['user']->uid;
  $result = db_query("SELECT n.reddit_access_token,
    n.reddit_refresh_token, n.reddit_token_expire_time
    FROM {reddit_oauth_tokens} n WHERE uid = %d", $uid);

  if (!$result) {
    watchdog('reddit_oauth', 'User %user did not have a refersh token to fetch.',
      array('%user' => $uid), WATCHDOG_ERROR);
    return FALSE;
  }

  $refresh_token_data = db_fetch_object($result);

  return $refresh_token_data;
}

/**
 * Save Reddit Oauth tokens.
 *
 * Requires that the Reddit OAuth table is already created for the user.
 */
function reddit_oauth_save_tokens($uid, $access_token, $refresh_token, $duration = 3600,
  $reddit_name = '', $reddit_id = '', $update = FALSE) {
  // Insert or update the new token information
  // Add the token duration to the current time
  // so we know when to renew the token.
  $expire_time = time() + $duration;

  // Check that the length is not over 255 characters for the tokens.
  if (strlen($access_token) > 255 || strlen($refresh_token) > 255) {
    watchdog('reddit_oauth', "Reddit token size larger than 255 characters.", array(), WATCHDOG_ERROR);
    $token_size_error = t("Unable to store user access tokens from Reddit.  Reddit token length is longer than 255 characters. Please notify the administrator of this website.");
    drupal_set_message($token_size_error, 'error', FALSE);

    return FALSE;
  }

  $record = array(
    'uid' => $uid,
    'reddit_access_token' => $access_token,
    'reddit_refresh_token' => $refresh_token,
    'reddit_token_expire_time' => $expire_time);

  // Only update name and id if needed.
  if ($reddit_name) {
    $record['reddit_name'] = $reddit_name;
  }
  if ($reddit_id) {
    $record['reddit_id'] = $reddit_id;
  }

  if ($update) {
    return drupal_write_record('reddit_oauth_tokens', $record, 'uid');
  }
  else {
    return drupal_write_record('reddit_oauth_tokens', $record);
  }
}

/**
 * Search for a user based on Reddit ID.
 */
function reddit_oauth_get_reddit_user($reddit_id) {
  $result = db_query("SELECT n.uid FROM {reddit_oauth_tokens} n WHERE
    reddit_id = '%s'", $reddit_id);

  if (!$result) {
    watchdog('reddit_oauth', 'Reddit user %user did not have a uid to fetch.',
      array('%user' => $reddit_id), WATCHDOG_ERROR);
    return FALSE;
  }

  $uid = db_fetch_object($result);
  return $uid;
}
