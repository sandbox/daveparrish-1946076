<?php

/**
 * @file
 * Administrative pages and functions for Reddit OAuth module.
 */

/**
 * Menu callback; Display the settings form for Reddit OAuth.
 */
function reddit_oauth_settings_form(&$form_state) {
  $form['reddit_oauth_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#size' => 20,
    '#maxlengh' => 50,
    '#description' => t('To use Reddit Authentication, a Reddit Application must be created. Set up your app in <a href="https://ssl.reddit.com/prefs/apps/">my apps</a> on Reddit.') . ' ' . t('Enter your App ID here.'),
    '#default_value' => variable_get('reddit_oauth_id', ''),
  );
  $form['reddit_oauth_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App Secret'),
    '#size' => 40,
    '#maxlengh' => 50,
    '#description' => t('To use Reddit Authentication, a Reddit Application must be created. Set up your app in <a href="https://ssl.reddit.com/prefs/apps/">my apps</a> on Reddit.') . ' ' . t('Enter your App Secret here.'),
    '#default_value' => variable_get('reddit_oauth_secret', ''),
  );
  global $base_url;
  $form['reddit_oauth_redirect_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URI'),
    '#disabled' => TRUE,
    '#size' => 40,
    '#maxlengh' => 50,
    '#description' => t('To use Reddit Authentication, a Reddit Application must be created. Copy this redirect URI into the redirect URI field on your <a href="https://ssl.reddit.com/prefs/apps/">Reddit App</a>.'),
    '#default_value' => "$base_url/reddit_oauth/connect",
  );

  $form['reddit_oauth_anonymous_authentication'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow anonymous authentication to Reddit'),
    '#description' => t('Allow users to connect to Reddit and associated features without first logging into Drupal.  WARNING: This feature does not currently work if Drupal caching is turned on.'),
    '#default_value' => variable_get('reddit_oauth_anonymous_authentication', FALSE),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Form submission function for reddit_oauth_settings_form().
 */
function reddit_oauth_settings_form_submit($form, &$form_state) {
  variable_set('reddit_oauth_id', $form_state['values']['reddit_oauth_id']);
  variable_set('reddit_oauth_secret', $form_state['values']['reddit_oauth_secret']);
  variable_set('reddit_oauth_redirect_uri', $form['reddit_oauth_redirect_uri']['#default_value']);
  variable_set('reddit_oauth_anonymous_authentication',
    $form_state['values']['reddit_oauth_anonymous_authentication']);

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Menu callback; Link Reddit and Drupal accounts.
 */
function reddit_oauth_link_accounts_form(&$form_state) {
  $form['reddit_oauth_link_accounts_choice'] = array(
    '#type' => 'radios',
    '#title' => t('Link Reddit account'),
    '#options' => array(
      'create' => t('Create an new account'),
      'login' => t('Login to an existing account'),
      'none' => t("Don't link my Reddit access information")),
    '#description' => t('In order to preserve your Reddit access we need to create an account for you.  Please decide if you you would like to create a new account or link an existing account.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Form submission function for reddit_oauth_link_accounts_form().
 */
function reddit_oauth_link_accounts_form_submit($form, &$form_state) {
  if ($form_state['values']['reddit_oauth_link_accounts_choice'] == 'create') {
    drupal_set_message(t('Please create an account so your Reddit access information can be saved.'));
    drupal_goto('user/register');
  }
  elseif ($form_state['values']['reddit_oauth_link_accounts_choice'] == 'login') {
    drupal_set_message(t('Please login to your account so your Reddit access information can be saved.'));
    drupal_goto('user/login');
  }
  elseif ($form_state['values']['reddit_oauth_link_accounts_choice'] == 'none') {
    // Should redirect to where the user was before they authenticated with
    // reddit.
    drupal_goto();
  }
}
