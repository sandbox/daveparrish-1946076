<?php

/**
 * @file
 * Provides functions used during Reddit OAuth action process.
 */

/**
 * Menu callback; Catches redirects back from Reddit when connecting.
 */
function reddit_oauth_reddit_connect() {
  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.helper');

  // If user is logged in and access_token is valid AND permenant
  // then we don't need to be here.
  if (reddit_oauth_is_connected() &&
      !(reddit_oauth_reddit_connection_type() == RedditOauthTokenType::TEMPORARY &&
      user_is_logged_in())) {
    watchdog('reddit_oauth', 'User @uid is already connected. Doesn\'t need to connect again.',
      array('@uid' => $GLOBALS['user']->uid));

    $reddit_id = reddit_oauth_reddit_name_load();
    return t('Already connected to Reddit as user @reddit_id', array('@reddit_id' => $reddit_id));
  }

  $return_string = "";

  if (isset($_GET["error"])) {
    $return_string .= ("<pre>OAuth Error: " . $_GET["error"] . "\n");
    $return_string .= ('<a href="index.php">Retry</a></pre>');
    watchdog('reddit_oauth', 'Reddit returned an OAuth error: @error',
      array('@error' => $_GET["error"]));
    return $return_string;
  }

  $authorize_url = 'https://ssl.reddit.com/api/v1/authorize';
  $access_token_url = 'https://ssl.reddit.com/api/v1/access_token';
  $client_id = variable_get('reddit_oauth_id', '');
  $client_secret = variable_get('reddit_oauth_secret', '');

  // Make sure to try and get the redirect uri from settings because https and
  // http might be switching.  Reddit is requires the ENTIRE URI to stay the same.
  $reddit_redirect_url = variable_get('reddit_oauth_redirect_uri',
    $GLOBALS['base_url'] . "/reddit_oauth/connect");

  _reddit_oauth_include_php_oauth2_library();

  $client = new OAuth2\Client($client_id, $client_secret, OAuth2\Client::AUTH_TYPE_AUTHORIZATION_BASIC);

  // Set the user agent as suggested by Reddit API Rules.
  // https://github.com/reddit/reddit/wiki/API.
  // TODO: Include the current version of this module.
  // TODO: Add any additional modules which add functionality to Reddit OAuth.
  $client->setCurlOption(CURLOPT_USERAGENT, 'Reddit OAuth Drupal module (dev version) by /u/dmp1ce');

  if (!isset($_GET["code"])) {
    // Generate state parameter (unguessable)
    // by hashing current time and a random number.
    $csrf_check = substr(hash("sha256", (time() . rand())), 0, 7);

    // Combine csrf_check and redirect into the state.
    $state = array('csrf_check' => $csrf_check, 'redirect' => $_GET['return_url']);
    $state_encoded = json_encode($state);

    // Save the csrf_check in the session so we can verify it when it comes
    // back.
    $_SESSION['reddit_oauth']['csrf_check'] = $state['csrf_check'];

    // Get the scope required from all plugin modules.
    $scopes_required_array = module_invoke_all('reddit_oauth_scope');
    $scopes_required = '';
    foreach ($scopes_required_array as $scope_string) {
      $scopes_required .= $scope_string . ',';
    }
    $scopes_required = substr($scopes_required, 0, -1);

    if (user_is_logged_in()) {
      $authentication_duration = 'permanent';
    }
    else {
      $authentication_duration = 'temporary';
    }

    $auth_url = $client->getAuthenticationUrl($authorize_url, $reddit_redirect_url,
      array(
        "scope" => $scopes_required,
        "state" => $state_encoded,
        'duration' => $authentication_duration,
      ));
    drupal_goto($auth_url);
  }
  else {
    // Look at the state parameter to see if everything checks out.
    $state = json_decode($_GET['state']);

    if ($state->csrf_check != $_SESSION['reddit_oauth']['csrf_check']) {
      watchdog('reddit_oauth', 'csrf Check failed!  Possible cross site scripting attack.');
      return t('Sorry, there was an error with the connection to Reddit.  Please try again.');
    }
    // Clear the csrf_check session data after check.
    unset($_SESSION['reddit_oauth']['csrf_check']);

    $params = array("code" => $_GET["code"], "redirect_uri" => $reddit_redirect_url);

    // TODO: Add a state parameter, like above.
    $response = $client->getAccessToken($access_token_url, "authorization_code", $params);

    $access_token_result = $response["result"];

    // Get the reddit identification so users can be logged in, in the future.
    $response_identity = reddit_oauth_reddit_action("/api/v1/me", array(), "GET",
      $access_token_result['access_token']);

    // With both reddit information and Drupal information,
    // Save this token to the users account.
    if (!user_is_logged_in()) {
      // If the user exists then log them in.
      $reddit_user_uid_result
        = reddit_oauth_get_reddit_user($response_identity['result']['id']);
      if ($reddit_user_uid_result) {
        // Log the user in.
        drupal_set_message('Found the user!');
        $account = user_load($reddit_user_uid_result->uid);
        user_external_login($account);

        // Redirect to the URL in the state.
        drupal_goto($state->redirect);
      }

      // If the user doesn't exist then ask user what to do
      // Create a user for them OR log into a user that already exists.
      // First save the important Reddit data in the user's session.
      $_SESSION['reddit_oauth']['response_identity'] = $response_identity;
      $_SESSION['reddit_oauth']['access_token_result'] = $access_token_result;
      $_SESSION['reddit_oauth']['access_token_expire_time']
        = time() + $access_token_result['expires_in'];
      drupal_goto('reddit_oauth/link_account');
    }

    global $user;
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
    reddit_oauth_save_tokens($user->uid, $access_token_result['access_token'],
      $access_token_result['refresh_token'], $access_token_result['expires_in'],
      $response_identity['result']['name'], $response_identity['result']['id']);

    // Clear temporary sessions that aren't needed anymore.
    unset($_SESSION['reddit_oauth']);

    $return_string = '<strong>Response for fetch me.json:</strong><pre>';
    $return_string .= print_r($access_token_result, TRUE);
    $return_string .= '</pre>';

    drupal_set_message(t('You have successfully connected with Reddit! You can deauthorize your account at any time in your <a href="/user">account settings</a>.'));

    // Redirect to the URL in the state.
    drupal_goto($state->redirect);
  }

  return $return_string;
}

/**
 * Deauthorize Drupal from being able to access Reddit API for user.
 */
function reddit_oauth_reddit_deauthorize() {
  // Simply remove the tokens that allow the system to access the users Reddit
  // account.
  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
  reddit_oauth_remove_tokens();

  // Remove any Reddit session variables.
  unset($_SESSION['reddit_oauth']);

  // Also will want to let the user know to remove the app access from their
  // reddit account.
  drupal_set_message(t('We removed access to your Reddit account.  You may also want to delete our App from your Reddit account here: https://ssl.reddit.com/prefs/apps/'));

  // Return to the previous URL.
  drupal_goto($_GET['return_url']);
}

/**
 * Refresh an access token (if needed).
 *
 * @return string
 *   Returns the refreshed access token
 *   OR returns FALSE if there was an error.
 */
function reddit_oauth_access_token_refresh($uid = NULL) {
  $uid = isset($uid) ? $uid : $GLOBALS['user']->uid;
  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
  $refresh_token_data = reddit_oauth_access_token_expire_time($uid);

  if (time() <= $refresh_token_data->reddit_token_expire_time) {
    // The token has not expired yet, so don't do anything.
    return $refresh_token_data->reddit_access_token;
  }

  $refresh_token_url = 'https://www.reddit.com/api/v1/access_token';
  $client_id = variable_get('reddit_oauth_id', '');
  $client_secret = variable_get('reddit_oauth_secret', '');

  _reddit_oauth_include_php_oauth2_library();

  $client = new OAuth2\Client($client_id, $client_secret,
    OAuth2\Client::AUTH_TYPE_AUTHORIZATION_BASIC);

  // Set the user agent as suggested by Reddit API Rules.
  // https://github.com/reddit/reddit/wiki/API.
  // TODO: Include the current version of this module.
  // TODO: Add any additional modules which add functionality to Reddit OAuth.
  $client->setCurlOption(CURLOPT_USERAGENT,
    'Reddit OAuth Drupal module (dev version) by /u/dmp1ce');

  // Make sure to try and get the redirect uri from settings because https and
  // http might be switching.  Reddit is requires the ENTIRE URI to stay the same.
  $reddit_redirect_url = variable_get('reddit_oauth_redirect_uri',
    $GLOBALS['base_url'] . "/reddit_oauth/connect");

  $params = array(
    "refresh_token" => $refresh_token_data->reddit_refresh_token,
  );
  $response = $client->getAccessToken($refresh_token_url, 'refresh_token', $params);

  if ($response['code'] != 200) {
    watchdog('reddit_oauth', 'Response from Reddit.com for refreshed tokens failed with '
      . $response['code'] . ' code.');
    drupal_set_message(t('Failed to renew token with Reddit.com.  Please try again.  If the error continues, please contact the administrator.'));
    return FALSE;
  }

  if (isset($response['result']['error'])) {
    watchdog('reddit_oauth', 'Response from Reddit.com for refreshed tokens failed with '
      . $response['result']['error'] . ' error.');
    drupal_set_message(t('Failed to renew token with Reddit.com.  Please try again.  If the error continues, please contact the administrator.'));
    return FALSE;
  }

  $access_token_result = $response["result"];

  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
  reddit_oauth_save_tokens($uid, $access_token_result['access_token'],
    $access_token_result['refresh_token'], $access_token_result['expires_in'],
    NULL, NULL, TRUE);

  return $access_token_result['access_token'];
}

/**
 * Framework for running a generic Reddit Oauth action.
 *
 * All Oauth actions should call this function.
 *
 * @param string $action
 *   The oauth action provided by the Reddit API.
 *   http://www.reddit.com/dev/api/oauth
 * @param array $parameters
 *   URL parameters.
 * @param string $method
 *   HTTP method (GET, POST)
 *
 * @return string
 *   Return the output returned by Reddit. OR return FALSE if not connected to
 *   Reddit.
 */
function reddit_oauth_reddit_action($action, $parameters = array(), $method = "GET",
    $access_token = "") {

  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.helper');

  // If not connected, then we shouldn't be here.
  if (empty($access_token) && !reddit_oauth_is_connected()) {
    return FALSE;
  }

  // Use passed in $access_token if not empty.
  if (!$access_token) {
    if (reddit_oauth_token_refreshable()) {
      // Attempt to refresh the permanent token.
      $access_token = reddit_oauth_access_token_refresh();
    }
    else {
      $access_token = reddit_oauth_access_token_load();
    }
  }

  _reddit_oauth_include_php_oauth2_library();

  // TODO: Verify that these variables have been set.
  $client_id = variable_get('reddit_oauth_id', '');
  $client_secret = variable_get('reddit_oauth_secret', '');

  $client = new OAuth2\Client($client_id, $client_secret, OAuth2\Client::AUTH_TYPE_AUTHORIZATION_BASIC);

  // Set the user agent as suggested by Reddit API Rules.
  // https://github.com/reddit/reddit/wiki/API.
  // TODO: Include the current version of this module.
  // TODO: Add any additional modules which add functionality to Reddit OAuth.
  $client->setCurlOption(CURLOPT_USERAGENT, 'Reddit OAuth Drupal module (dev version) by /u/dmp1ce');

  $client->setAccessToken($access_token);
  $client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);

  $response = NULL;
  try {
    $response = $client->fetch("https://oauth.reddit.com$action", $parameters, $method);
  }
  catch (OAuth2\Exception $e) {
    $error_message = t("Error: '@error'", array('@error' => $e->getMessage())) . '<br />' .
      t("Most likely we could not connect to Reddit.com. Please try again in a few mintues if Reddit is experiencing downtime. Otherwise please contract this website's administrator.");
    watchdog('reddit_oauth', "Error from OAuth request: '@error'", array('@error' => $e->getMessage()));
    drupal_set_message($error_message, 'error', FALSE);
  }

  // TODO: Log any errors before returning.
  return $response;
}

/**
 * Menu callback; Shows the user there Reddit identity information.
 */
function reddit_oauth_reddit_identity() {
  $response = reddit_oauth_reddit_action("/api/v1/me");
  if (!$response) {
    return t('Sorry, you must be connected to Reddit to view identity.');
  }
  $client_identity_result = $response['result'];

  $return_string = '<strong>Response for fetch me.json:</strong><pre>';
  $return_string .= print_r($client_identity_result, TRUE);
  $return_string .= '</pre>';

  return $return_string;
}

/**
 * Include PHP-OAuth2 library.
 *
 * @return bool
 *   TRUE if the library was included successfully. Otherwise FALSE.
 */
function _reddit_oauth_include_php_oauth2_library() {
  $php_oauth2_path = libraries_get_path('PHP-OAuth2');

  if (!$php_oauth2_path) {
    watchdog('reddit_oauth', 'PHP-OAuth2 library is missing!');
    return FALSE;
  }

  require_once $php_oauth2_path . "/Client.php";
  require_once $php_oauth2_path . "/GrantType/IGrantType.php";
  require_once $php_oauth2_path . "/GrantType/AuthorizationCode.php";
  require_once $php_oauth2_path . "/GrantType/RefreshToken.php";

  return TRUE;
}
