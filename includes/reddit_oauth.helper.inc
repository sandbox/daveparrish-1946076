<?php

/**
 * @file
 * Various helper functions
 */

/**
 * Returns TRUE if the PHP-OAuth library exists.
 */
function reddit_oauth_php_oauth_exists() {
  return libraries_get_path('PHP-OAuth2') ? TRUE : FALSE;
}

/**
 * Determines if the user is connected to Reddit via OAuth.
 *
 * @return bool
 *   TRUE if the user is logged in to Reddit via OAuth else FALSE.
 */
function reddit_oauth_is_connected() {
  // Look for a token to see if user is authenticated with Reddit.
  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
  $reddit_id = reddit_oauth_access_token_sql_load();
  if ($reddit_id) {
    return TRUE;
  }

  // Check session variable for temporary Reddit authentication.
  if (isset($_SESSION['reddit_oauth']['access_token_expire_time']) &&
      $_SESSION['reddit_oauth']['access_token_expire_time'] > time()) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Loads the access token whether it is in the DB or session.
 */
function reddit_oauth_access_token_load() {
  if (isset($_SESSION['reddit_oauth']['access_token_result']['access_token'])) {
    return $_SESSION['reddit_oauth']['access_token_result']['access_token'];
  }
  else {
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
    return reddit_oauth_access_token_sql_load();
  }
}
/**
 * Loads the reddit name whether it is in the DB or session.
 */
function reddit_oauth_reddit_name_load() {
  if (isset($_SESSION['reddit_oauth']['response_identity']['result']['name'])) {
    return $_SESSION['reddit_oauth']['response_identity']['result']['name'];
  }
  else {
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.sql');
    return reddit_oauth_reddit_name_sql_load();
  }
}

class RedditOauthTokenType {
  const PERMANENT = 0;
  const TEMPORARY = 1;
}

/**
 * Returns the type of token being used from Reddit.
 *
 * @return string
 *   Return type of token. Either 'permanent' or 'temporary'.
 */
function reddit_oauth_reddit_connection_type() {
  if (isset($_SESSION['reddit_oauth']['access_token_result'])) {
    return RedditOauthTokenType::TEMPORARY;
  }
  return RedditOauthTokenType::PERMANENT;
}

/**
 * Determines if the token is refreshable.
 *
 * This doesn't necessary mean that the token is expired.  It only means that
 * the token is permenant and can be refreshed.
 *
 * @return string
 *   Return TRUE if the token needs to be refreshed.  FALSE otherwise.
 */
function reddit_oauth_token_refreshable() {
  $access_token = reddit_oauth_access_token_load();
  if ($access_token &&
      reddit_oauth_reddit_connection_type() == RedditOauthTokenType::PERMANENT) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
