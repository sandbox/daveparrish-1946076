Welcome to the Reddit OAuth README.txt.  Hopefully, this document help you setup
Reddit OAuth to work with your Reddit account and make your experience a with
Reddit OAuth a pleasant one.  Enjoy!

### Requirements
-   libcurl ( Must be accessable via PHP.  The curl Drupal module will enforce
    this requirement. )

### Setup

1. Setup Reddit app for Reddit OAuth to connect with
    -   Make a note of App ID and Secret
    -   Enter the redirect uri as `$base_url/reddit_oauth/connect` where
        `$base_url` is your Drupal URI.  For example
        `http://mydrupalsite.com/reddit_oauth/connect`.  The redirect uri that
        Drupal will send to Reddit is shown in the
        `/admin/settings/reddit_oauth` page for convience.  That URI should be
        identical to what is in the Reddit redirect uri field.
2. Enter App ID and Secret in the Drupal Reddit OAuth settings.

### Troubleshooting
-   If you are getting an invalid redirect uri error from Reddit then you have
    probably entered in a different URI than is shown on the
    `/admin/settings/reddit_oauth` page.  This URI need to match exactly.
