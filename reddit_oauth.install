<?php

/**
 * @file
 * Provides install and update functions for the Reddit OAuth module.
 */

/**
 * Implements hook_requirements().
 */
function reddit_oauth_requirements($phase) {
  if ($phase == 'runtime') {
    // Get the correct translation function.
    $t = get_t();

    $return = array(
      'Reddit OAuth' => array(
        'title' => $t('Reddit OAuth'),

      ),
      'PHP-OAuth2 Library' => array(
        'title' => $t('PHP-OAuth2 Library'),
      ));

    // Make sure both Reddit ID and Secret are set.
    if (variable_get('reddit_oauth_id', NULL) == NULL ||
      variable_get('reddit_oauth_secret', NULL) == NULL) {

      $return['Reddit OAuth']['description'] = $t('Reddit OAuth requires a Reddit App ID and Secret.
            Please enter the App ID and secret
            <a href="https://ssl.reddit.com/prefs/apps/">from Reddit.com</a>
            and enter them in the <a href="@drupal-settings">Drupal settings</a>.',
            array('@drupal-settings' => url('admin/settings/reddit_oauth')));
      $return['Reddit OAuth']['severity'] = REQUIREMENT_ERROR;
      $return['Reddit OAuth']['value'] = 'Missing Reddit App ID and/or Secret';

    }
    else {
      $return['Reddit OAuth']['value'] = $t('<a href="@drupal-settings">Configured</a>',
        array('@drupal-settings' => url('admin/settings/reddit_oauth')));
    }

    // Make sure the PHP-OAuth2 library is installed.
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.helper');
    if (!reddit_oauth_php_oauth_exists()) {
      $return['PHP-OAuth2 Library']['description']
        = $t('Reddit OAuth requires the <a href="#">PHP-OAuth2 Library</a> to be installed for the Reddit OAuth2 module, but this library could not be found.');
      $return['PHP-OAuth2 Library']['severity'] = REQUIREMENT_ERROR;
      $return['PHP-OAuth2 Library']['value'] = 'Missing PHP-OAuth2 library';
    }
    else {
      $return['PHP-OAuth2 Library']['value'] = 'Library found';
    }

    return $return;
  }
}

/**
 * Implements hook_install().
 */
function reddit_oauth_install() {
  drupal_install_schema('reddit_oauth');
}

/**
 * Implements hook_uninstall().
 */
function reddit_oauth_uninstall() {
  drupal_uninstall_schema('reddit_oauth');
  variable_del('reddit_oauth_id');
  variable_del('reddit_oauth_secret');
}

/**
 * Implements hook_schema().
 */
function reddit_oauth_schema() {
  $schema['reddit_oauth_tokens'] = array(
    'fields' => array(
      'uid' => array(
        'description' => 'Drupal User ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'reddit_access_token' => array(
        'description' => 'The Reddit Oauth access token',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'reddit_refresh_token' => array(
        'description' => 'The Reddit Oauth refresh token',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'reddit_token_expire_time' => array(
        'description' => 'The Unix timestamp when the token should be refreshed.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'reddit_name' => array(
        'description' => "The Reddit user's name",
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'reddit_id' => array(
        'description' => "The Reddit user's ID",
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('uid'),
    // There should never be more than one reddit_id assigned for all users.
    'unique keys' => array(
      'reddit_id' => array('reddit_id'),
    ),
    'indexes' => array(
      'uid_reddit_access_token' => array('uid', 'reddit_access_token'),
      'uid_reddit_refresh_token' => array('uid', 'reddit_access_token'),
      'uid_reddit_expire_time' => array('uid', 'reddit_token_expire_time'),
      'uid_reddit_id' => array('uid', 'reddit_id'),
    ),
  );
  return $schema;
}

/**
 * Remove all rows with truncated tokens and increase varchar length to 255.
 */
function reddit_oauth_update_6001() {
  // Remove all the rows that probably had their tokens truncated.
  db_query('DELETE FROM {reddit_oauth_tokens} WHERE LENGTH(reddit_access_token) = 32 OR LENGTH(reddit_refresh_token) = 32');

  $ret = array();
  // Remove the keys and indexes because of the field change.
  // https://api.drupal.org/api/drupal/includes%21database.pgsql.inc/function/db_change_field/6
  db_drop_primary_key($ret, 'reddit_oauth_tokens');
  db_drop_unique_key($ret, 'reddit_oauth_tokens', 'reddit_id');
  db_drop_index($ret, 'reddit_oauth_tokens', 'uid_reddit_access_token');
  db_drop_index($ret, 'reddit_oauth_tokens', 'uid_reddit_refresh_token');
  db_drop_index($ret, 'reddit_oauth_tokens', 'uid_reddit_expire_time');
  db_drop_index($ret, 'reddit_oauth_tokens', 'uid_reddit_id');

  // Change 'reddit_access_token'.
  db_change_field($ret, 'reddit_oauth_tokens', 'reddit_access_token', 'reddit_access_token',
    array(
      'description' => 'The Reddit Oauth access token',
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 255,
      'default' => '',
    )
  );

  // Change 'reddit_refresh_token' and rebuild the keys and indexes.
  db_change_field($ret, 'reddit_oauth_tokens', 'reddit_refresh_token', 'reddit_refresh_token',
    array(
      'description' => 'The Reddit Oauth refresh token',
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',),
    array(
      'primary key' => array('uid'),
      // There should never be more than one reddit_id assigned for all users.
      'unique keys' => array(
        'reddit_id' => array('reddit_id'),
      ),
      'indexes' => array(
        'uid_reddit_access_token' => array('uid', 'reddit_access_token'),
        'uid_reddit_refresh_token' => array('uid', 'reddit_access_token'),
        'uid_reddit_expire_time' => array('uid', 'reddit_token_expire_time'),
        'uid_reddit_id' => array('uid', 'reddit_id'),
      ),));
}
